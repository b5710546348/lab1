package oop.lab1;

//TODO Write class Javadoc
/**
 * A simple model for a student with a name and id.
 * 
 * @author Patchara Pattiyathanee
 */
public class Student extends Person {
	private long id;
	
	//TODO Write constructor Javadoc
	public Student(String name, long id) {
		super(name); // name is managed by Person
		this.id = id;
	}

	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	}

	//TODO Write equals
	public boolean equals(Object obj) {
			if (obj == null) {
				return false;
			}
			if ( obj.getClass() != this.getClass() ){
				return false;
			}
			Student other = (Student)obj;
			if ( this.id == other.id ){
				return true;
			}	
		return false; 
	}
	
}
